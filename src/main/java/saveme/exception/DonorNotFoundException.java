package saveme.exception;

public class DonorNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public DonorNotFoundException(Long id) {
		super(String.format("Donor Not found! (Donor id: %d)", id));
	}
}
