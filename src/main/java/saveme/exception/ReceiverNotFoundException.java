package saveme.exception;

public class ReceiverNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public ReceiverNotFoundException(Long id) {
		super(String.format("Receiver Not found! (Receiver id: %d)", id));
	}
}
