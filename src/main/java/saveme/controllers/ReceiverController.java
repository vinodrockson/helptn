package saveme.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;

import saveme.assemblers.DonorAssembler;
import saveme.assemblers.ReceiverAssembler;
import saveme.exception.DonorNotFoundException;
import saveme.exception.ReceiverNotFoundException;
import saveme.models.Donor;
import saveme.models.Receiver;
import saveme.repositories.DonorRepository;
import saveme.repositories.ReceiverRepository;
import saveme.resources.DonorResource;
import saveme.resources.ReceiverResource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.maps.DistanceMatrixApi;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DistanceMatrix;

@RestController
@RequestMapping("/rest/receiver")
public class ReceiverController {
	@Autowired
	DonorRepository dRepo;
	
	@Autowired
	ReceiverRepository rRepo;
	
	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<ReceiverResource> registerReceiver(
			@RequestBody ReceiverResource res) throws RestClientException, ReceiverNotFoundException, JsonProcessingException{

		ResponseEntity<ReceiverResource> resp;
		
		if (res == null) {
			throw new ReceiverNotFoundException(null);
		}
		
		Receiver rec = new Receiver();
		rec.setRname(res.getRname());
		rec.setRaddress(res.getRaddress());
		rec.setRphone(res.getRphone());
		rec.setRtype(res.getRtype());
		rec.setRpersons(res.getRpersons());
		rec.setDonorid(res.getDonorid());
		Date date = new Date();
		rec.setRegDate(date);		
		rec = rRepo.saveAndFlush(rec);
		String temprid = rec.getId().toString();

		String[] dids =  res.getDonorid().split(",");
		String rid;
		for(int i=0; i<dids.length; i++){			
			Donor don = dRepo.findOne(Long.parseLong(dids[i]));
			rid = don.getReceiverid();

			if(rid == null || rid.indexOf(temprid) == -1){
				if(rid == null){
					rid = temprid + ",";
				}
				else{
					rid = rid + temprid + ",";
				}
				don.setReceiverid(rid);
				dRepo.saveAndFlush(don);
			}
		}

		
		ReceiverAssembler dassem = new ReceiverAssembler();
		ReceiverResource newRes = dassem.toResource(rec);		
	
		resp = new ResponseEntity<ReceiverResource>(
				newRes, HttpStatus.OK);
		return resp;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/list")
	@ResponseStatus(HttpStatus.OK)
	public List<ReceiverResource> getReceivers(@RequestParam("phone") String phone) {
		List<Receiver> newrec = new ArrayList<Receiver> ();
		List<Donor> dons = dRepo.findByPhone(phone);
		System.out.println("dons size");
		System.out.println(dons.size());
		if(dons.size() >= 1){
			for(int j=0; j<dons.size(); j++){
				String recids = dons.get(j).getReceiverid();
				if (recids != null){
					String[] recid = recids.split(","); 
					for(int k=0; k<recid.length; k++){
						Receiver rec = rRepo.findOne(Long.parseLong(recid[k]));
						if(newrec == null || newrec.contains(rec) == false){	
							newrec.add(rec);
						}
					}
				}
			}
		}
		ReceiverAssembler dassem = new ReceiverAssembler();
		List<ReceiverResource> res = dassem.toResource(newrec);
		return res;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/all")
	@ResponseStatus(HttpStatus.OK)
	public List<ReceiverResource> getAllReceivers() {

		List<Receiver> rec = rRepo.findAll();
		System.out.println(rec.size());
		ReceiverAssembler dassem = new ReceiverAssembler();
		List<ReceiverResource> res = dassem.toResource(rec);
		return res;
	}
	
	@ExceptionHandler(ReceiverNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handPONotFoundException(ReceiverNotFoundException ex) {
		
	}
}
