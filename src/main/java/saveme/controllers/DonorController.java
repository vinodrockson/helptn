package saveme.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.model.Distance;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.DistanceMatrixRow;

import saveme.assemblers.DonorAssembler;
import saveme.assemblers.ReceiverAssembler;
import saveme.exception.DonorNotFoundException;
import saveme.models.Donor;
import saveme.models.Receiver;
import saveme.repositories.DonorRepository;
import saveme.resources.DonorResource;
import saveme.resources.ReceiverResource;

@RestController
@RequestMapping("/rest/donor")
public class DonorController {
	
	@Autowired
	DonorRepository dRepo;
	
	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<DonorResource> registerDonor(
			@RequestBody DonorResource res) throws RestClientException, DonorNotFoundException, JsonProcessingException{

		ResponseEntity<DonorResource> resp;
		
		if (res == null) {
			throw new DonorNotFoundException(null);
		}
		
		Donor don = new Donor();
		don.setPhone(res.getPhone());
		don.setPlace(res.getPlace());
		don.setType(res.getType());
		don.setPersons(res.getPersons());
		Date date = new Date();
		don.setStartDate(date);
		
		don = dRepo.saveAndFlush(don);
		
		DonorAssembler dassem = new DonorAssembler();
		DonorResource newRes = dassem.toResource(don);		
	
		resp = new ResponseEntity<DonorResource>(
				newRes, HttpStatus.OK);
		return resp;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "")
	@ResponseStatus(HttpStatus.OK)
	public List<DonorResource> getDonor(@RequestParam("place") String place, @RequestParam("persons") int persons, @RequestParam("type") String type) {

		List<String> oritemp = new ArrayList<String>();
		List<Donor> sdonnew = new ArrayList<Donor>();	
		List<Donor> sdon = dRepo.findDonor(persons, type);
		for(int i=0;i<sdon.size();i++){
			oritemp.add(sdon.get(i).getPlace());
		}
		String[] origins = new String[ oritemp.size() ];
		oritemp.toArray( origins );		
		String[] destinations = new String[] {place};
		if(sdon.size() > 1){
			GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyBZpEiQIUJP9pxmzzHBlXrLFtXc5SgfZhY");
		    DistanceMatrix matrix = null;
			try {
				matrix = DistanceMatrixApi.getDistanceMatrix(context,origins,destinations).await();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	
			for(int j=0;j<matrix.rows.length;j++){
				for(int k=0;k<matrix.rows[j].elements.length;k++){
					Long dis = matrix.rows[j].elements[k].distance.inMeters;
					System.out.println(dis);
					if(dis < 20000){
						for(int h=0;h<sdon.size();h++){
							if(sdon.get(h).getPlace().compareTo(oritemp.get(j)) == 0){
								sdonnew.add(sdon.get(h));
								}
						}
					}
				}
				
			}
		}
		DonorAssembler dassem = new DonorAssembler();
		List<DonorResource> res = dassem.toResource(sdonnew);
		return res;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/alldonors")
	@ResponseStatus(HttpStatus.OK)
	public List<DonorResource> getAllDonor() {

		List<Donor> dons = dRepo.findAll();
		System.out.println(dons.size());
		DonorAssembler dassem = new DonorAssembler();
		List<DonorResource> res = dassem.toResource(dons);
		return res;
	}

	@ExceptionHandler(DonorNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handPONotFoundException(DonorNotFoundException ex) {
		
	}

}
