package saveme.models;


import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Entity
@Data
@XmlRootElement
public class Donor {

	@Id
	@GeneratedValue
	Long id;
	
	String phone;
	String place;
	String type;
	int persons;
	
	@Temporal(TemporalType.DATE)
	Date startDate;
	
	String receiverid;
}
