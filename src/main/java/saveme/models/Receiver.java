package saveme.models;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Entity
@Data
@XmlRootElement
public class Receiver {

	
	@Id
	@GeneratedValue
	Long id;
	
	String rname;
	String raddress;
	String rphone;	
	int rpersons;
	String rtype;
	String donorid;
	
	@Temporal(TemporalType.DATE)
	Date regDate;
}
