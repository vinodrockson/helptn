package saveme.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import saveme.models.Receiver;

@Repository
public interface ReceiverRepository extends JpaRepository<Receiver, Long> {

}
