package saveme.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import saveme.models.Donor;

@Repository
public interface DonorRepository extends JpaRepository<Donor, Long> {
	
	@Query("select d from Donor d where d.persons >= :persons and d.type = :type")
	List<Donor> findDonor(@Param("persons") int persons, @Param("type") String type);
	
	@Query("select d from Donor d where d.phone = :phone")
	List<Donor> findByPhone(@Param("phone") String phone);
}
