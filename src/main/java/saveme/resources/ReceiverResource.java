package saveme.resources;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import saveme.util.ResourceSupport;
import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
//@XmlRootElement(name = "receiver")
public class ReceiverResource extends ResourceSupport {
	
	Long idres;
	
	String rname;
	String raddress;
	String rphone;	
	int rpersons;
	String rtype;	
	String donorid;
	
	@Temporal(TemporalType.DATE)
	Date regDate;
	
}
