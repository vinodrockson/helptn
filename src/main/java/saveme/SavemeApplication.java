package saveme;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.WebMvcProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import saveme.util.CustomResponseErrorHandler;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class SavemeApplication {
	
	@Autowired
	private WebMvcProperties mvcProperties = new WebMvcProperties();
	
	@Bean
	public RestTemplate restTemplate() {
		RestTemplate _restTemplate = new RestTemplate();
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(new MappingJackson2HttpMessageConverter());
		_restTemplate.setMessageConverters(messageConverters);
		_restTemplate.setErrorHandler(new CustomResponseErrorHandler());
		return _restTemplate;
	}

    public static void main(String[] args) {
        SpringApplication.run(SavemeApplication.class, args);
    }
}
