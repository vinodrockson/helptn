package saveme.assemblers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import saveme.controllers.ReceiverController;
import saveme.models.Receiver;
import saveme.resources.ReceiverResource;

public class ReceiverAssembler extends ResourceAssemblerSupport<Receiver, ReceiverResource> {

	public ReceiverAssembler() {
		super(ReceiverController.class, ReceiverResource.class);
		}
	
	@Override
	public ReceiverResource toResource(Receiver receiver) {
		ReceiverResource receiverResource = createResourceWithId(
			receiver.getId(), receiver);
		receiverResource.setIdres(receiver.getId());
		receiverResource.setRname(receiver.getRname());
		receiverResource.setRphone(receiver.getRphone());
		receiverResource.setRaddress(receiver.getRaddress());
		receiverResource.setRtype(receiver.getRtype());
		receiverResource.setRegDate(receiver.getRegDate());
		receiverResource.setRpersons(receiver.getRpersons());
		receiverResource.setDonorid(receiver.getDonorid());
		return receiverResource;	
	}

	public List<ReceiverResource> toResource(List<Receiver> receiverList) {
	List<ReceiverResource> receiverRes = new ArrayList<>();
	
	for (Receiver sup : receiverList) {
		receiverRes.add(toResource(sup));
	}
	
	return receiverRes;
	}
}

