package saveme.assemblers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import saveme.controllers.DonorController;
import saveme.models.Donor;
import saveme.resources.DonorResource;

public class DonorAssembler extends ResourceAssemblerSupport<Donor, DonorResource> {

	public DonorAssembler() {
		super(DonorController.class, DonorResource.class);
		}
	
	@Override
	public DonorResource toResource(Donor donor) {
		DonorResource donorResource = createResourceWithId(
			donor.getId(), donor);
		donorResource.setIdres(donor.getId());
		donorResource.setPhone(donor.getPhone());
		donorResource.setPlace(donor.getPlace());
		donorResource.setType(donor.getType());
		donorResource.setStartDate(donor.getStartDate());
		donorResource.setPersons(donor.getPersons());
		return donorResource;	
	}

	public List<DonorResource> toResource(List<Donor> donorList) {
	List<DonorResource> donorRes = new ArrayList<>();
	
	for (Donor sup : donorList) {
		donorRes.add(toResource(sup));
	}
	
	return donorRes;
	}
}
