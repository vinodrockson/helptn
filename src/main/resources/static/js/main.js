var app = angular.module('project', ['ngRoute','ui.bootstrap']);

app.config(function($routeProvider) {
	$routeProvider
	    .when('/', {
	      controller: 'BookingController',
	      templateUrl: 'views/saveme/main.html'
	    })
	    .when('/donor', {
	      controller: 'BookingController',
	      templateUrl: 'views/saveme/donor.html'
	    })
       .when('/receiver', {
	      controller: 'BookingController',
	      templateUrl: 'views/saveme/receiver.html'
	    })
	    .otherwise({
	      redirectTo:'/'
	    });
	});