/*
Template: Twist - Responsive coming soon page
Author: The Theme Lab
Email: saif.shajib@gmail.com
AuthorUrl: http://themeforest.net/user/thethemelab 
*/

  "use strict";
   /*Background Image Settings*/
   var image = "images/bg1.jpg";
             
  /*Onload function*/
  $(window).load(function () {
      $("#preload").css({
          display: 'none'
      });
      $("body").removeClass("preload");
    
      /* Title Rotator */
      var options = {
          "speed": 4000, // Rotate every 4 seconds
          "transition_speed": 500, // Fade in/out has a .5 second duration
          "sub_selector": ".rotate"
      };
      $("#rotate").rotator(options);
      
      $(document).on("keypress", 'form', function (e) {
    	    var code = e.keyCode || e.which;
    	    if (code == 13) {
    	        e.preventDefault();
    	        return false;
    	    }
    	});

  });


  // Add background 
  $.backstretch([image]);  


  function isDesktop() {
      if ($(window).width() >= 768) return true;
      else return false;
  }

  