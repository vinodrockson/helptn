var app = angular.module('project');

app.directive('googleplace', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, model) {
            var options = {
                types: [],
                componentRestrictions: {country: 'in'}
            };
            scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

            google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
                scope.$apply(function() {
                    model.$setViewValue(element.val());                
                });
            });
        }
    };
});

app.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});

app.controller('BookingController', ['$scope', 'filterFilter', '$http', '$location','$route', '$window', function($scope, filterFilter , $http, $location, $route, $window) {
	var temptype = "";
	
	
	console.log($location.path() == "/");
	$scope.catalogShown = false;
	$scope.newapply = true;
	$scope.donsuccess = false;
	$scope.noData = false;
	$scope.donreg = true;
	$scope.showlist = false;
	
	$scope.searchDonor = function () {
       	document.getElementsByName("searchdon").noValidate = false;
		$scope.people = "";
    	var per = $scope.spersons;
    	var pla = $scope.splace;
    	var stype = "";    	
    	var numper = "";
    	var temp = "";

    	
    	if($scope.setype === "Other"){
    		temptype = $scope.sother;
    		if($scope.sother === "" || $scope.sother === undefined || $scope.sother === null){
        		stype = "false";
        	    document.getElementById("sother").setCustomValidity("Please fill other need");
    		}    		
    	}
    	else {
    		temptype = $scope.setype;
    	}
    	
    	if (pla === "" || pla === undefined || pla === null){
    		pla = "false";
     	}
    	
    	if (temptype === "not"){
    		stype = "false";
    	    document.getElementById("setype").setCustomValidity("Please select any one");
    	}
    	
    	if (per === "" || per === undefined || per === null){
    		numper = "false";
    		document.getElementById("spersons").setCustomValidity("Please enter greater number");
    	}

     	if (pla !== "false" && stype !== "false" && numper !== "false" ){
        $http.get('/rest/donor', {params: {place: $scope.splace, persons: per, type: temptype}}).success(function(data, status, headers, config) {
        	if ((data || []).length === 0) {
//    			console.log("No data");
         			$scope.catalogShown = false;
        			$scope.noData = true;
		        	$scope.success = false;
        		}
        		else{
//        			console.log(JSON.stringify(data));
        			for(z=0;z<data.length;z++){
        				var g = data[z];
        				temp = temp + g["idres"] + ",";
        			}
        			$scope.dids = temp;
        			$scope.people = data.length;
		        	$scope.catalogShown = true;
		        	$scope.noData = false;
        		}
        });
        }
       	document.getElementsByName("searchdon").noValidate = true;
    };
	
   
		  
    $scope.registerDonor = function () { 
    	document.getElementsByName("dapply").noValidate = false;
    	var per = $scope.persons;
    	var plac = $scope.place;
    	var pnum = "";
    	var stype = "";
    	
    	var numper = "";
    	
    	if($scope.type === "Other"){
    		temptype = $scope.other;
    		if($scope.other === "" || $scope.other === undefined || $scope.other === null){
        		stype = "false";
        	    document.getElementById("other").setCustomValidity("Please fill other contribution");
    		}    		
    	}
    	else {
    		temptype = $scope.type;
    	}
    	
    	if (/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[123456789]\d{9}$/.test($scope.phone) === false){
    		pnum = "false";
    		document.getElementById("phone2").setCustomValidity("Invalid mobile number");
    	}
    	
    	if (temptype === "not"){
    		stype = "false";
    		document.getElementById("type").setCustomValidity("Please select one");
    	}
    	
    	if (per === "" || per === undefined || per === null){
    		numper = "false";
    		document.getElementById("persons").setCustomValidity("Please enter greater number");
    	}
    	
    	if (plac === "" || plac === undefined || plac === null){
    		plac = "false";
     	}
    	
       	if (pnum !== "false" && plac !== "false" && stype !== "false" && numper !== "false" ){
		    register = { phone: $scope.phone, place: $scope.place, type: temptype, persons: per};
		    $http.post('/rest/donor', register).success(function(data, status, headers, config) {	            
//		        	console.log(data);
		        	$scope.newapply = false;
		        	$scope.donsuccess = true;		        	
		        });
       }
       	document.getElementsByName("dapply").noValidate = true;
	};
	

	
	  $scope.showmyList = function () { 
		  
	    	var pnum = "";
	    	$scope.rlist = [];
	       	document.getElementsByName("dquery").noValidate = false;
	    	if (/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[123456789]\d{9}$/.test($scope.aphone) === false){
	    		pnum = "false";
	    		document.getElementById("phone1").setCustomValidity("Invalid mobile number");
	    	}
	    	
	       	if (pnum !== "false"){			    
			    $http.get('/rest/receiver/list', {params: {phone:$scope.aphone}}).success(function(data, status, headers, config) {
		        	$scope.donreg = false;
		        	$scope.showlist = true;
		        	$scope.showlistbut = true;
		        	$scope.showmylistbut = false;
		        	
		        	if ((data || []).length === 0) {
//		    			console.log("No data");
			        	$scope.rectable = false;
			        	$scope.norecs = true;

		        		}
	        		else{
	        			  $scope.rlist = data;
//	        			  console.log(JSON.stringify($scope.rlist));	
	        				$scope.resetFilters = function () {
	        					// needs to be a function or it won't trigger a $watch
	        					$scope.search = {};
	        				};

	        				// pagination controls
	        				$scope.currentPage = 1;
	        				$scope.totalItems = $scope.rlist.length;
	        				$scope.entryLimit = 3; // items per page
	        				$scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

	        				// $watch search to update pagination
	        				$scope.$watch('search', function (newVal, oldVal) {
	        					$scope.filtered = filterFilter($scope.rlist, newVal);
	        					$scope.totalItems = $scope.filtered.length;
	        					$scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
	        					$scope.currentPage = 1;
	        				}, true);
	        				
				        	$scope.rectable = true;
				        	$scope.norecs = false;
		        		}

		        });
	       }
	       	document.getElementsByName("dquery").noValidate = true;
		};
		$scope.home = function () {
			$window.location.href = '/';
		}
		
		$scope.showfull = function () { 
		    	$scope.rlist = [];
	    
				    $http.get('/rest/receiver/all').success(function(data, status, headers, config) {
			        	if ((data || []).length === 0) {
//			    			console.log("No data");
				        	$scope.rectable = false;
				        	$scope.norecs = true;

			        		}
		        		else{
		        			  $scope.rlist = data;
//		        			  console.log(JSON.stringify($scope.rlist));	
		        				$scope.resetFilters = function () {
		        					// needs to be a function or it won't trigger a $watch
		        					$scope.search = {};
		        				};

		        				// pagination controls
		        				$scope.currentPage = 1;
		        				$scope.totalItems = $scope.rlist.length;
		        				$scope.entryLimit = 3; // items per page
		        				$scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

		        				// $watch search to update pagination
		        				$scope.$watch('search', function (newVal, oldVal) {
		        					$scope.filtered = filterFilter($scope.rlist, newVal);
		        					$scope.totalItems = $scope.filtered.length;
		        					$scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
		        					$scope.currentPage = 1;
		        				}, true);
					        	$scope.rectable = true;
					        	$scope.norecs = false;
			        		}
			        	$scope.donreg = false;
			        	$scope.showlist = true;
			        	$scope.showlistbut = false;
			        	$scope.showmylistbut = true;
			        });

			};
			
	
    $scope.registerReceiver = function (dids) { 
//    	console.log(dids);
    	var per = $scope.spersons;
    	var pl = $scope.splace;
    	var pnum = "";
    	var stype = "";
    	
    	var numper = "";
       	document.getElementsByName("apply").noValidate = false;
       	
    	if($scope.setype === "Other"){
    		temptype = $scope.sother;
    		if($scope.sother === "" || $scope.sother === undefined || $scope.sother === null){
        		stype = "false";
        	    document.getElementById("sother").setCustomValidity("Please fill other need");
    		}    		
    	}
    	else {
    		temptype = $scope.setype;
    	}
    	
    	if (/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[123456789]\d{9}$/.test($scope.rphone) === false){
    		pnum = "false";
    		document.getElementById("rphone").setCustomValidity("Invalid mobile number");
    	}
    	
    	if (temptype === "not"){
    		stype = "false";
    		document.getElementById("setype").setCustomValidity("Pleast select one");
    	}
    	
    	if (per === "" || per === undefined || per === null){
    		numper = "false";
    		document.getElementById("spersons").setCustomValidity("Please enter greater number");
    	}
    	if (pl === "" || pl === undefined || pl === null){
    		pl = "false";
     	}
    	
       	if (pnum !== "false" && pl !== "false" && stype !== "false" && numper !== "false" ){
		    register = {rname: $scope.rname, raddress: $scope.splace, rphone: $scope.rphone, rtype: temptype, rpersons: per, donorid : dids};
		    $http.post('/rest/receiver', register).success(function(data, status, headers, config) {	            
//		        	console.log(data);
		        	$scope.catalogShown = false;
		        	$scope.success = true;
		        });
       }
       	document.getElementsByName("apply").noValidate = true;
	};
}]);


